package com.nlmk.evteev.tm;

import com.nlmk.evteev.tm.controller.ProjectController;
import com.nlmk.evteev.tm.controller.SystemController;
import com.nlmk.evteev.tm.controller.TaskController;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.service.ProjectService;
import com.nlmk.evteev.tm.service.TaskService;

import java.util.Scanner;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

/**
 * Основной класс
 */
public class Application {

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final ProjectController projectController = new ProjectController(projectService);
    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskService taskService = new TaskService(taskRepository);
    private final TaskController taskController = new TaskController(taskService);
    private final SystemController systemController = new SystemController();

    /**
     * Точка входа
     *
     * @param args дополнительные аргументы запуска
     */
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final Application application = new Application();
        application.systemController.displayWelcome();
        application.run(args);
        String command = "";

        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            application.run(command);
            System.out.println();
        }
    }

    /**
     * Основной метод исполнения
     *
     * @param args дополнительные параметры запуска
     */
    public void run(final String[] args) {
        if (args == null || args.length < 1) return;
        final String param = args[0];
        run(param);
    }

    /**
     * Обработка консольного ввода
     *
     * @param cmdString команда с консоли
     * @return код выполнения
     */
    public int run(final String cmdString) {
        if (cmdString == null || cmdString.isEmpty()) return -1;
        switch (cmdString) {
            case CMD_VERSION:
                return systemController.displayVersion();
            case CMD_HELP:
                return systemController.displayHelp();
            case CMD_ABOUT:
                return systemController.displayAbout();
            case CMD_EXIT:
                systemController.displayExit();
            case PROJECT_CREATE:
                return projectController.createProject();
            case PROJECT_CLEAR:
                return projectController.clearProject();
            case PROJECT_LIST:
                return projectController.listProject();
            case PROJECT_VIEW:
                return projectController.viewProjectByIndex();
            case PROJECT_REMOVE_BY_ID:
                return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_NAME:
                return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_INDEX:
                return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX:
                return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID:
                return projectController.updateProjectById();
            case TASK_CREATE:
                return taskController.createTask();
            case TASK_CLEAR:
                return taskController.clearTask();
            case TASK_LIST:
                return taskController.listTask();
            case TASK_VIEW:
                return taskController.viewTaskByIndex();
            case TASK_REMOVE_BY_ID:
                return taskController.removeTaskById();
            case TASK_REMOVE_BY_NAME:
                return taskController.removeTaskByName();
            case TASK_REMOVE_BY_INDEX:
                return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_ID:
                return taskController.updateTaskById();
            case TASK_UPDATE_BY_INDEX:
                return taskController.updateTaskByIndex();
            default:
                return systemController.displayError();
        }
    }

}
