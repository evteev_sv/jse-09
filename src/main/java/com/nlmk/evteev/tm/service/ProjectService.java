package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.repository.ProjectRepository;

import java.util.List;

/**
 * Сервисный класс для работы с репозиторием проектов
 */
public class ProjectService {

    private ProjectRepository projectRepository;

    /**
     * Конструктор класса
     *
     * @param projectRepository репозиторий проектов
     */
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /**
     * Создание проекта и добавление его в список проектов
     *
     * @param name имя проекта
     * @return объект типа {@link Project}
     */
    public Project create(String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.create(name);
    }

    /**
     * Очистка списка проектов
     */
    public void clear() {
        projectRepository.clear();
    }

    /**
     * Возвращает список проектов
     *
     * @return список проектов {@link List}
     */
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    /**
     * Поиск проекта по индексу
     *
     * @param index индекс проекта
     * @return проект {@link Project}
     */
    public Project findByIndex(int index) {
        if (index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    /**
     * Поиск проекта по наименованию
     *
     * @param name наименование проекта
     * @return проект {@link Project}
     */
    public Project findByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.findByName(name);
    }

    /**
     * Поиск проекта по коду
     *
     * @param id код проекта
     * @return проект {@link Project}
     */
    public Project findById(Long id) {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    /**
     * Удаление проекта по коду
     *
     * @param id код проекта
     * @return проект, который был удален {@link Project}
     */
    public Project removeById(Long id) {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }

    /**
     * Удален епроекта по индексу
     *
     * @param index индекс проекта
     * @return проект, который был удален {@link Project}
     */
    public Project removeByIndex(int index) {
        if (index < 0) return null;
        return projectRepository.removeByIndex(index);
    }

    /**
     * Удаление проекта по названию.
     *
     * @param name название проекта
     * @return проект, который был удален {@link Project}
     */
    public Project removeByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.removeByName(name);
    }

    /**
     * Изменение проекта
     *
     * @param id          код проекта
     * @param name        наименование проекта
     * @param description описание проекта
     * @return проект, который изменен {@link Project}
     */
    public Project update(Long id, String name, String description) {
        if (id == null || name == null || name.isEmpty() || description == null)
            return null;
        return projectRepository.update(id, name, description);
    }

}
