package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.repository.TaskRepository;

import java.util.List;

/**
 * Сервисный класс для работы с репозиторием задач
 */
public class TaskService {

    private TaskRepository taskRepository;

    /**
     * Конструктор класса
     *
     * @param taskRepository репозиторий задач {@link TaskRepository}
     */
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Создание задачи и добавление ее в список задач
     *
     * @param name имя задачи
     * @return объект типа {@link Task}
     */
    public Task create(String name) {
        if (name == null || name.isEmpty())
            return null;
        return taskRepository.create(name);
    }

    /**
     * Очистка списка задач
     */
    public void clear() {
        taskRepository.clear();
    }

    /**
     * Возвращает список задач
     *
     * @return список задач {@link List}
     */
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    /**
     * Поиск задачи по индексу
     *
     * @param index индекс задачи
     * @return задача {@link Task}
     */
    public Task findByIndex(int index) {
        if (index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    /**
     * Поиск задачи по наименованию
     *
     * @param name наименование задачи
     * @return задача {@link Task}
     */
    public Task findByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return taskRepository.findByName(name);
    }

    /**
     * Поиск задачи по коду
     *
     * @param id колд задачи
     * @return задача {@link Task}
     */
    public Task findById(Long id) {
        if (id == null) return null;
        return taskRepository.findById(id);
    }

    /**
     * Удаление задачи по коду
     *
     * @param id код задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeById(Long id) {
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    /**
     * Удаление задачи по наименованию
     *
     * @param name наименование задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeByName(String name) {
        if (name == null || name.isEmpty())
            return null;
        return taskRepository.removeByName(name);
    }

    /**
     * Удаление задачи по индексу
     *
     * @param index индекс задачи
     * @return удаленная задача {@link Task}
     */
    public Task removeByIndex(Integer index) {
        if (index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    /**
     * Изменение задачи
     *
     * @param id          код задачи
     * @param name        наименование задачи
     * @param description описание задачи
     * @return измененная задача {@link Task}
     */
    public Task update(Long id, String name, String description) {
        if (id == null || name == null || name.isEmpty() || description == null)
            return null;
        return taskRepository.update(id, name, description);
    }

}
