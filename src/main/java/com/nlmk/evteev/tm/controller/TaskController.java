package com.nlmk.evteev.tm.controller;

import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.service.TaskService;

/**
 * Класс контроллера задач.
 */
public class TaskController extends AbstractController {

    private final TaskService taskService;

    /**
     * Конструктор класса контроллера задач
     *
     * @param taskService сервисный класс задач {@link TaskService}
     */
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    /**
     * Изменение проекта по коду
     *
     * @return код исполнения
     */
    public int updateProjectById() {
        System.out.println("[Update project by id]");
        System.out.println("Введите код проекта: ");
        final Long vId = Long.getLong(scanner.nextLine());
        System.out.println("Введите новое имя проекта: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание проекта: ");
        final String description = scanner.nextLine();
        if (taskService.update(vId, name, description) != null) {
            System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }
        return 0;
    }

    /**
     * Изменение задачи по индексу
     *
     * @return код исполнения
     */
    public int updateTaskByIndex() {
        System.out.println("[Update task by index]");
        System.out.println("Введите индекс задачи: ");
        final int vId = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task = taskService.findByIndex(vId);
        if (task == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("Введите новое название задачи: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание задачи: ");
        final String description = scanner.nextLine();
        if (taskService.update(task.getId(), name, description) != null) {
            System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }
        return 0;
    }

    /**
     * Изменение задачи по коду
     *
     * @return код исполнения
     */
    public int updateTaskById() {
        System.out.println("[Update task by id]");
        System.out.println("Введите код задачи: ");
        final Long vId = Long.getLong(scanner.nextLine());
        System.out.println("Введите новое название задачи: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание задачи: ");
        final String description = scanner.nextLine();
        if (taskService.update(vId, name, description) != null) {
            System.out.println("[OK]");
        } else {
            System.out.println("[FAIL]");
        }
        return 0;
    }


    /**
     * Удаление задачи по названию
     *
     * @return код исполнения
     */
    public int removeTaskByName() {
        System.out.println("[Remove task by name]");
        System.out.println("Введите имя задачи: ");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Удаление задачи по коду
     *
     * @return код исполнения
     */
    public int removeTaskById() {
        System.out.println("[Remove task by id]");
        System.out.println("Введите ID задачи: ");
        final Long vId = scanner.nextLong();
        final Task task = taskService.removeById(vId);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Удаление задачи по индексу
     *
     * @return код исполнения
     */
    public int removeTaskByIndex() {
        System.out.println("[Remove task by index]");
        System.out.println("Введите индекс задачи: ");
        final Integer vId = scanner.nextInt();
        final Task task = taskService.removeByIndex(vId);
        if (task == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[OK]");
        }
        return 0;
    }

    /**
     * Просмотр задачи в консоли
     *
     * @param task задача {@link com.nlmk.evteev.tm.entity.Task}
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        System.out.println("[View Task]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
    }

    /**
     * Просмотр задачи по индексу
     *
     * @return код исполнения
     */
    public int viewTaskByIndex() {
        System.out.println("Введите индекс задачи: ");
        final int index = scanner.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        viewTask(task);
        return 0;
    }

    /**
     * Создание задачи
     *
     * @return код выполнения
     */
    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Укажите имя задачи: ");
        final String lv_name = scanner.nextLine();
        taskService.create(lv_name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Очистка задачи
     *
     * @return код выполнения
     */
    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Список задач
     *
     * @return код выполнения
     */
    public int listTask() {
        System.out.println("[LIST PROJECT]");
        System.out.println(taskService.findAll());
        System.out.println("[OK]");
        return 0;
    }

}
